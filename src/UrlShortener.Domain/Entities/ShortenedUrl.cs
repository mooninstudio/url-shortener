namespace UrlShortener.Domain.Entities;

public class ShortenedUrl
{
    public string Id { get; set; }
    public string TargetUrl { get; set; }
    public string ShortUrl { get; set; }
}