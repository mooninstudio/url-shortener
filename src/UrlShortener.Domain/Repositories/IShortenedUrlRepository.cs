using UrlShortener.Domain.Entities;

namespace UrlShortener.Domain.Repositories;

public interface IShortenedUrlRepository
{
    IEnumerable<ShortenedUrl> GetAll();
    ShortenedUrl Get(string id);
    void Insert(ShortenedUrl shortenedUrl);
    void Save();
}