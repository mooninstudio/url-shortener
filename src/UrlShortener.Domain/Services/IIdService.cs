namespace UrlShortener.Domain.Services;

public interface IIdService
{
    string GenerateId();
}