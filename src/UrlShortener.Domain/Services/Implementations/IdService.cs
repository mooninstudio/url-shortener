namespace UrlShortener.Domain.Services.Implementations;

public class IdService : IIdService
{
    public string GenerateId()
    {
        return Convert
            .ToBase64String(Guid.NewGuid().ToByteArray())
            .TrimEnd('=')
            .Replace('/', 'A')
            .Replace('+', 'A');
    }
}