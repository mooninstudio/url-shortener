using Microsoft.AspNetCore.Http;
using UrlShortener.Domain.Entities;
using UrlShortener.Domain.Exceptions;
using UrlShortener.Domain.Repositories;

namespace UrlShortener.Domain.Services.Implementations;

public class UrlShortenerService : IUrlShortenerService
{
    private readonly IShortenedUrlRepository _shortenedUrlRepository;
    private readonly IIdService _idService;
    private readonly IHttpContextAccessor _httpContextAccessor;
    
    public UrlShortenerService(
        IShortenedUrlRepository shortenedUrlRepository,
        IIdService idService,
        IHttpContextAccessor httpContextAccessor)
    {
        _shortenedUrlRepository = shortenedUrlRepository;
        _idService = idService;
        _httpContextAccessor = httpContextAccessor;
    }

    public IEnumerable<ShortenedUrl> GetAll()
    {
        return _shortenedUrlRepository.GetAll();
    }

    public ShortenedUrl? Get(string id)
    {
        try
        {
            return _shortenedUrlRepository.Get(id);
        }
        catch (EntityNotFoundException)
        {
            return null;
        }
    }
    
    public ShortenedUrl ShortenUrl(string url)
    {
        var id = _idService.GenerateId();
        var shortenedUrl = new ShortenedUrl
        {
            Id = id,
            ShortUrl = GetShortenedUrl(id),
            TargetUrl = url
        };
        
        _shortenedUrlRepository.Insert(shortenedUrl);
        _shortenedUrlRepository.Save();

        return shortenedUrl;
    }

    public bool ValidateUrl(string url) 
    {
        return Uri.TryCreate(url, UriKind.Absolute, out _);
    }

    private string GetShortenedUrl(string id)
    {
        var context = _httpContextAccessor.HttpContext;

        return $"{context.Request.Scheme}://{context.Request.Host}/{id}";
    }
}