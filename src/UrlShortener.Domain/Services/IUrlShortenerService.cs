using UrlShortener.Domain.Entities;

namespace UrlShortener.Domain.Services;

public interface IUrlShortenerService
{
    IEnumerable<ShortenedUrl> GetAll();
    ShortenedUrl? Get(string id);
    ShortenedUrl ShortenUrl(string url);
    bool ValidateUrl(string url);
}