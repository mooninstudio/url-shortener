using System.ComponentModel.DataAnnotations;
using UrlShortener.Domain.Entities;

namespace UrlShortener.WebApplication.Models;

public class ShortenUrlViewModel
{
    [Required]
    public string? Url { get; set; }
    public string? ShortUrl { get; set; }
    public List<ShortenedUrl> ShortenedUrls { get; set; } = new();
}