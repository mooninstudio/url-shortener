using Microsoft.AspNetCore.Mvc;
using UrlShortener.Domain.Services;

namespace UrlShortener.WebApplication.Controllers;

public class RedirectController : Controller
{
    private readonly IUrlShortenerService _urlShortenerService;
    
    public RedirectController(IUrlShortenerService urlShortenerService)
    {
        _urlShortenerService = urlShortenerService;
    }

    public IActionResult Index(string id)
    {
        var shortenedUrl = _urlShortenerService.Get(id);

        if (shortenedUrl == null)
            return RedirectToAction("Index", "ShortenUrl");
        
        return Redirect(shortenedUrl.TargetUrl);
    }
}