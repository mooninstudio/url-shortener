using Microsoft.AspNetCore.Mvc;
using UrlShortener.Domain.Services;
using UrlShortener.WebApplication.Models;

namespace UrlShortener.WebApplication.Controllers;

public class ShortenUrlController : Controller
{
    private readonly IUrlShortenerService _urlShortenerService;

    public ShortenUrlController(IUrlShortenerService urlShortenerService)
    {
        _urlShortenerService = urlShortenerService;
    }
    
    public IActionResult Index()
    {
        return View();
    }
    
    public IActionResult ShortenUrl(ShortenUrlViewModel model)
    {
        if (!_urlShortenerService.ValidateUrl(model.Url))
        {
            ModelState.AddModelError("url", "Provided url is invalid");
            return View("Index", model);
        }
        
        var shortenedUrl = _urlShortenerService.ShortenUrl(model.Url);
        model.ShortUrl = shortenedUrl.ShortUrl;
        
        return View("ShortenedUrl", model);
    }

    public IActionResult List(ShortenUrlViewModel model)
    {
        model.ShortenedUrls = _urlShortenerService.GetAll().ToList();
        return View("ShortenedUrlList", model);
    }
}