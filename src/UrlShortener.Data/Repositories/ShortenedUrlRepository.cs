using UrlShortener.Domain.Entities;
using UrlShortener.Domain.Exceptions;
using UrlShortener.Domain.Repositories;

namespace UrlShortener.Data.Repositories;

public class ShortenedUrlRepository : IShortenedUrlRepository
{
    private UrlShortenerContext context;
    
    public ShortenedUrlRepository(UrlShortenerContext context)
    {
        this.context = context;
    }
    
    public IEnumerable<ShortenedUrl> GetAll()
    {
        return context.ShortenedUrls.ToList();
    }

    public ShortenedUrl Get(string id)
    {
        var entity = context.ShortenedUrls.Find(id);

        if (entity == null)
        {
            throw new EntityNotFoundException();
        }
        
        return entity;
    }

    public void Insert(ShortenedUrl shortenedUrl)
    {
        context.ShortenedUrls.Add(shortenedUrl);
    }

    public void Save()
    {
        context.SaveChanges();
    }
}