using Microsoft.EntityFrameworkCore;
using UrlShortener.Domain.Entities;

namespace UrlShortener.Data;

public class UrlShortenerContext : DbContext
{
    public DbSet<ShortenedUrl> ShortenedUrls { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase("url-shortener-db");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<ShortenedUrl>().HasKey(o => o.Id);
        modelBuilder.Entity<ShortenedUrl>().Property(o => o.Id).IsRequired();
        modelBuilder.Entity<ShortenedUrl>().Property(o => o.ShortUrl).IsRequired();
        modelBuilder.Entity<ShortenedUrl>().Property(o => o.TargetUrl).IsRequired();
    }
}